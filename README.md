# Eco-server.com Splash screen

### Build & push app

```
docker build -t registry.eco-server.com/ecosplashscreen .
docker push registry.eco-server.com/ecosplashscreen
```

### Start app

```
docker-compose -up
```