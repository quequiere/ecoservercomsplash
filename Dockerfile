FROM node:13.12.0-alpine as build

COPY . ./
RUN npm ci
#RUN npm install react-scripts@3.4.1 -g --silent
RUN npm run build


FROM nginx:stable-alpine
COPY --from=build /build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]