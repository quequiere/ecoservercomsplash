import React from 'react';
import globe from './globe.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={globe} className="App-logo" alt="globe" />
        <p>Welcome to eco-server.com</p>
        <p>An eco strange loop game hoster</p>
        <a
          className="App-link"
          href="https://discord.gg/Dj46mC7"
          target="_blank"
          rel="noopener noreferrer"
        >
          Ask for beta access
        </a>
      </header>
    </div>
  );
}

export default App;
